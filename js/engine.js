﻿jQuery(document).ready(function() {

$('.inner-menu li').hover(function() {
    $(this).children('ul').fadeToggle();
});
    // BEGIN of script for header-slider
var headerSlider  =  $('.header-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: true
});

$('.header-slider__next').click(function(){
 	$(headerSlider).slick("slickNext");
 });
 $('.header-slider__prev').click(function(){
 	$(headerSlider).slick("slickPrev");
});
    // END of script for top-slider

    // BEGIN of script for look-also__slider
    $('.look-also__slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
        responsive: [{
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
            },
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
            },
        }]
    });
    // END of script for look-also__slider

    // BEGIN of script for components-slider
    $('.components-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
         responsive: [{
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
            },
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
            },
        }]
    });
    // END of script for components-slider

    // Слайдер карточки товара
    var productSlider = $('.product-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.product-slider__nav',

    });
    $('.product-slider__nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product-slider',
        dots: false,
        arrows: false,
        focusOnSelect: true,
        vertical: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                vertical: false
            },
        }]

    });

// // Слайдер годов
    $('.history-slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
        centerMode: true,
        responsive: [{
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
            },
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
            },
        }]
    });
$('.history-slider').on('afterChange', function(event, slick, currentSlide){   
  $(".history-slider__item").removeClass('history-slider__item--last');
  var currentInfo = $('.history-slider__item.slick-current .history-slider__info').clone();
$('.history-slider__text--current').empty().append(currentInfo);
console.log(currentInfo);
});


// BEGIN of script to stick header
    $(window).scroll(function(){
    	var headerHeight = $(".header-overlay").height();
        var sticky = $('.header-overlay'),
            scroll = $(window).scrollTop();
        if (scroll > headerHeight) {
            sticky.addClass('header-fixed');
        }
        else{
        	sticky.removeClass('header-fixed');
        }
    });

// END of script to stick header







// Бургер меню
$('.burger-menu').click(function() {
    $(this).toggleClass('active');
    $(this).parent().parent().parent('.header-top').toggleClass('active');
    $('.header-overlay').toggleClass('z-index');
});


// Позиционирование скрола вниз
var windowWidth = $(window).width();
var containerWidth = $(".container").width();
$('.header-scroll__bottom').css("right", (windowWidth - containerWidth) / 2);

// Позиционирование стрелок слайдера
var windowWidth = $(window).width();
var containerWidth = $(".container").width();
$('.header-slider__btn').css("left", (windowWidth - containerWidth) / 2);


    // Скрипт скролла по ID
    $(".header-scroll__bottom").on("click", function(event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();

        //забираем идентификатор бока с атрибута href
        var id = $(this).attr('href'),

            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top;

        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({ scrollTop: top }, 1000);
    });
    // End of scroll nav

    //  Скролл вверх
    $('.scroll-top').on('click', function(e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });


    // BEGIN of script for tabs.
    $('.tabs-nav a').click(function() {
        var _id = $(this).attr('href');
        var _targetElement = $(this).parents('.warehouses').find('#' + _id);
        $('.tabs-nav a').removeClass('active');
        $(this).addClass('active');
        $('.tabs-item').removeClass('active');
        _targetElement.addClass('active');
        return false;

    });


    $('.tabs-mobile__btn').click(function() {
        var _targetElementParent = $(this).parents('.tabs-item');
        _targetElementParent.toggleClass('active-m');
        _targetElementParent.find('.tabs-item__body').slideToggle(600);
        return false;
    });


// Скрипт блока "Наши услуги"
$('.services-box__item:not(.services-box__item--title)').hover(function() {
	_bg = $(this).attr('data-background');
	$('.services-box').css({"background":"url(" + _bg +")", "background-repeat": "no-repeat", "background-size": "cover"});
	$('.services-box__item').toggleClass("hover");
	$('.services-box__item > img').toggleClass('opacity');
});



  //Begin of GoogleMaps
 function initialize()
{
	var mapProp1 = {
    center: new google.maps.LatLng(44.038016, 43.121226),
    zoom:17,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000"},{"lightness":80}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000"},{"lightness":70},{"weight":1.2}]}],
    disableDefaultUI:true,
    zoomControl: true,
    scrollwheel: false,
    zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
    }
  };
  var mapProp2 = {
    center: new google.maps.LatLng(44.038016, 43.121226),
    zoom:17,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000"},{"lightness":80}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000"},{"lightness":70},{"weight":1.2}]}],
    disableDefaultUI:true,
    zoomControl: true,
    scrollwheel: false,
    zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
    }
  };
  var mapProp3 = {
    center: new google.maps.LatLng(44.038016, 43.121226),
    zoom:17,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000"},{"lightness":80}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000"},{"lightness":70},{"weight":1.2}]}],
    disableDefaultUI:true,
    zoomControl: true,
    scrollwheel: false,
    zoomControlOptions: {
    position: google.maps.ControlPosition.RIGHT_CENTER
    }
  };
  var mapProp4 = {
    center: new google.maps.LatLng(44.038016, 43.121226),
    zoom:17,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000"},{"lightness":80}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000"},{"lightness":70},{"weight":1.2}]}],
    disableDefaultUI:true,
    zoomControl: true,
    scrollwheel: false,
    zoomControlOptions: {
    position: google.maps.ControlPosition.RIGHT_CENTER
    }
  };

  var mapProp5 = {
    center: new google.maps.LatLng(44.038016, 43.121226),
    zoom:17,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000"},{"lightness":80}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000"},{"lightness":70},{"weight":1.2}]}],
    disableDefaultUI:true,
    zoomControl: true,
    scrollwheel: false,
    zoomControlOptions: {
    position: google.maps.ControlPosition.RIGHT_CENTER
    }
  };


  var map1 = new google.maps.Map(document.getElementById("googleMap1"),mapProp1);
  var myCenter=new google.maps.LatLng(44.038016, 43.121226);
  var marker;
  var marker=new google.maps.Marker({
    position:myCenter,
    icon: "css/img/marker.png"    
    });
  marker.setMap(map1);

  var map2 = new google.maps.Map(document.getElementById("googleMap2"),mapProp2);
  var myCenter=new google.maps.LatLng(44.038016, 43.121226);
  var marker;
  var marker=new google.maps.Marker({
    position:myCenter,
    icon: "css/img/marker.png"    
    });
  marker.setMap(map2);

  var map3 = new google.maps.Map(document.getElementById("googleMap3"),mapProp3);
   var myCenter=new google.maps.LatLng(44.038016, 43.121226);
  var marker;
  var marker=new google.maps.Marker({
    position:myCenter,
    icon: "css/img/marker.png"    
    });
  marker.setMap(map3);

  var map4 = new google.maps.Map(document.getElementById("googleMap4"),mapProp4);
   var myCenter=new google.maps.LatLng(44.038016, 43.121226);
  var marker;
  var marker=new google.maps.Marker({
    position:myCenter,
    icon: "css/img/marker.png"    
    });
  marker.setMap(map4);

 var map5 = new google.maps.Map(document.getElementById("googleMap5"),mapProp5);
   var myCenter=new google.maps.LatLng(44.038016, 43.121226);
  var marker;
  var marker=new google.maps.Marker({
    position:myCenter,
    icon: "css/img/marker.png"    
    });
  marker.setMap(map5);

}

google.maps.event.addDomListener(window, 'load', initialize);





});